window.meshviewer = SAGE2_App.extend
  init: (data) ->
    console.log "Mesh viewer started with data", data

    @SAGE2Init 'div', data
    @resizeEvents = 'continuous'

    # Be prepared to receive the file list.
    targetFile = data.state.file

    @lastDrawTime = data.date.getTime() / 1000

    $element = $(@element)
    $element.append """
<link href='https://fonts.googleapis.com/css?family=Ubuntu+Condensed' rel='stylesheet' type='text/css'>
<link href="/uploads/apps/mesh_viewer/css/fontello.css" rel="stylesheet"/>
<link href="/uploads/apps/mesh_viewer/css/animation.css" rel="stylesheet"/>
<link rel="stylesheet" type="text/css" href="/uploads/apps/mesh_viewer/css/playbackcontrols.css" />
<link rel="stylesheet" type="text/css" href="/uploads/apps/mesh_viewer/css/renderingcontrols.css" />
"""

    @engine = new MeshViewer.Engine
      width: @width
      height: @height
      $appWindow: $element
      resourcesPath: @resrcPath

    @needsResize = data.date

    # Start the draw loop.
    @_shouldQuit = false
    @drawLoop()

    onFileList (fileListData) =>
      if targetFile
        # Search for other files that only differ by the number index.
        filenameStartIndex = targetFile.lastIndexOf('/') + 1
        filename = targetFile.substring filenameStartIndex

        extension = filename.split('.').pop()

        if extension is 'top'
          # With .top files, look for all other .top files in the folder, since the data is distributed.
          filenames = []
          folder = targetFile.substring 0, filenameStartIndex

          for mesh in fileListData.meshes
            url = mesh.sage2URL

            # Only include other top files.
            meshExtension = url.split('.').pop()
            continue unless meshExtension is 'top'

            # Only include files in the same folder.
            meshFolder = url.substring 0, url.lastIndexOf('/') + 1
            continue unless folder is meshFolder

            filenames.push url

          @engine.load
            filenames: filenames

        else
          # For other files, look for files within the same number sequence.

          #console.log "SEARCHING FOR OTHER SISTER FILES OF", filename

          # First see how many numbers we have.
          numberStarts = []
          lastWasDigit = false

          for index in [0...filename.length]
            charCode = filename.charCodeAt index
            isDigit = 48 <= charCode <= 57

            # Is this the start of a number?
            if isDigit and not lastWasDigit
              numberStarts.push index

            lastWasDigit = isDigit

          #console.log "Numbers are at locations", numberStarts

          if numberStarts.length
            bestFrameSet =
              frames: {}
              frameLengths: {}
              length: 0

          else
            # This is not an animation sequence
            bestFrameSet =
              frames:
                0: true

              frameLengths:
                1: true

              startFrame: 0
              endFrame: 0
              length: 1
              numberStartIndex: -1

          for numberStartIndex in numberStarts
            frameSet =
              numberStartIndex: numberStartIndex
              frames: {}
              frameLengths: {}

            startString = targetFile.substring 0, filenameStartIndex + numberStartIndex
            originalFileNumber = null

            for mesh in fileListData.meshes
              url = mesh.sage2URL

              if url.indexOf(startString) is 0
                # Parse number from this location.
                numberString = ""
                index = filenameStartIndex + numberStartIndex
                while (48 <= url.charCodeAt(index) <= 57)
                  numberString += url.charAt index
                  index++

                continue unless numberString.length

                number = parseInt numberString
                unless frameSet.frames[number]
                  frameSet.frames[number] = true
                  frameSet.frameLengths[numberString.length] = true

              originalFileNumber = number if url is targetFile

            # Find start and end frame.
            startFrame = endFrame = originalFileNumber

            startFrame-- while frameSet.frames[startFrame-1]
            endFrame++ while frameSet.frames[endFrame+1]

            frameSet.startFrame = startFrame
            frameSet.endFrame = endFrame
            frameSet.length = endFrame - startFrame + 1

            #console.log "finished frame set", frameSet

            # This is the best candidate for a frame set if the range is longer.
            bestFrameSet = frameSet if frameSet.length > bestFrameSet.length

          #console.log "best frame set it", bestFrameSet

          # If frames had variable string length, consider them unpadded. If they were all the same length, pad to it.
          frameNumberLengths = _.keys bestFrameSet.frameLengths
          frameNumberLength = if frameNumberLengths.length > 1 then null else parseInt frameNumberLengths[0]

          # Replace the number in the filename with a hash.
          filename = targetFile

          if bestFrameSet.numberStartIndex > -1
            index = filenameStartIndex + bestFrameSet.numberStartIndex
            #console.log "removing things at index", index, filename.charAt index
            while 48 <= filename.charCodeAt(index) <= 57
              filename = "#{filename.substring(0, index)}#{filename.substring index + 1}"

            filename = "#{filename.substring(0, index)}##{filename.substring index}"

          #console.log "Loading animation with filename pattern", filename

          @engine.load
            filename: filename
            frameNumberLength: frameNumberLength
            startFrame: bestFrameSet.startFrame
            endFrame: bestFrameSet.endFrame

  load: (date) ->
    #your load code here- update app based on this.state

  draw: (date) ->
    @needsDraw = date

  resize: (date) ->
    @needsResize = date

  drawLoop: ->
    requestAnimationFrame =>
      @drawLoop() unless @_shouldQuit

    if (@needsResize)
      @width = @element.clientWidth
      @height = @element.clientHeight

      @engine.resize @width, @height

      $(@element).css
        fontSize: @height * 0.015

      @refresh @needsResize
      @needsResize = false

    if (@needsDraw)
      date = @needsDraw
      time = date.getTime() / 1000
      elapsedTime = time - @lastDrawTime
      @engine.draw elapsedTime
      @needsDraw = false
      @lastDrawTime = time

  event: (eventType, position, user_id, data, date) ->
    if eventType == 'pointerPress'
      @engine.onMouseDown position, data.button
      @refresh date
    else if eventType == 'pointerMove'
      @engine.onMouseMove position
      @refresh date
    else if eventType == 'pointerRelease'
      @engine.onMouseUp position, data.button
      @refresh date
    if eventType == 'pointerScroll'
      @engine.onMouseScroll data.wheelDelta
      @refresh date
    if eventType == 'keyboard'
      if data.character == ' '
        @engine.playbackControls.togglePlay()
        @refresh date
    if eventType == 'specialKey'
      if data.state is 'down'
        switch data.code
          when 37 # Left
            @engine.playbackControls.previousFrame()

          when 39 # Right
            @engine.playbackControls.nextFrame()

          when 83 # S
            @engine.toggleShadows()

          when 67 # C
            @engine.activeControls = @engine.cameraControls

          when 79 # O
            @engine.activeControls = @engine.rotateControls

          when 82 # R
            @engine.toggleReflections()

          when 65 # A
            @engine.toggleAmbientLight()

          when 68 # D
            @engine.toggleDirectionalLight()

          when 86 # V
            @engine.toggleVertexColors()

          when 87 # W
            @engine.toggleWireframe()

        @refresh date
    else if eventType == 'widgetEvent'
      switch data.identifier
        when 'Up'
          # up
          @engine.orbitControls.pan 0, @engine.orbitControls.keyPanSpeed
          @engine.orbitControls.update()
        when 'Down'
          # down
          @engine.orbitControls.pan 0, -@engine.orbitControls.keyPanSpeed
          @engine.orbitControls.update()
        when 'Left'
          # left
          @engine.orbitControls.pan @engine.orbitControls.keyPanSpeed, 0
          @engine.orbitControls.update()
        when 'Right'
          # right
          @engine.orbitControls.pan -@engine.orbitControls.keyPanSpeed, 0
          @engine.orbitControls.update()
        when 'ZoomIn'
          @engine.orbitControls.scale 4
        when 'ZoomOut'
          @engine.orbitControls.scale -4
        when 'Loop'
          @rotating = !@rotating
          @engine.orbitControls.autoRotate = @rotating
        else
          console.log 'No handler for:', data.identifier
          return

      @refresh date


  move: (date) ->
    # this.sage2_width, this.sage2_height give width and height of app in global wall coordinates
    # date: when it happened
    @refresh date

  quit: ->

    console.log "Destroying meshViewer"

    # Clean up EVERYTHING!
    @_shouldQuit = true
    @engine.destroy()
    @engine = null
    # It's the end
    @log 'Done'
