// Generated by CoffeeScript 1.10.0
(function() {
  window.meshviewer = SAGE2_App.extend({
    init: function(data) {
      var $element, targetFile;
      console.log("Mesh viewer started with data", data);
      this.SAGE2Init('div', data);
      this.resizeEvents = 'continuous';
      targetFile = data.state.file;
      this.lastDrawTime = data.date.getTime() / 1000;
      $element = $(this.element);
      $element.append("<link href='https://fonts.googleapis.com/css?family=Ubuntu+Condensed' rel='stylesheet' type='text/css'>\n<link href=\"/uploads/apps/mesh_viewer/css/fontello.css\" rel=\"stylesheet\"/>\n<link href=\"/uploads/apps/mesh_viewer/css/animation.css\" rel=\"stylesheet\"/>\n<link rel=\"stylesheet\" type=\"text/css\" href=\"/uploads/apps/mesh_viewer/css/playbackcontrols.css\" />\n<link rel=\"stylesheet\" type=\"text/css\" href=\"/uploads/apps/mesh_viewer/css/renderingcontrols.css\" />");
      this.engine = new MeshViewer.Engine({
        width: this.width,
        height: this.height,
        $appWindow: $element,
        resourcesPath: this.resrcPath
      });
      this.needsResize = data.date;
      this._shouldQuit = false;
      this.drawLoop();
      return onFileList((function(_this) {
        return function(fileListData) {
          var bestFrameSet, charCode, endFrame, extension, filename, filenameStartIndex, filenames, folder, frameNumberLength, frameNumberLengths, frameSet, i, index, isDigit, j, k, l, lastWasDigit, len, len1, len2, mesh, meshExtension, meshFolder, number, numberStartIndex, numberStarts, numberString, originalFileNumber, ref, ref1, ref2, ref3, ref4, startFrame, startString, url;
          if (targetFile) {
            filenameStartIndex = targetFile.lastIndexOf('/') + 1;
            filename = targetFile.substring(filenameStartIndex);
            extension = filename.split('.').pop();
            if (extension === 'top') {
              filenames = [];
              folder = targetFile.substring(0, filenameStartIndex);
              ref = fileListData.meshes;
              for (i = 0, len = ref.length; i < len; i++) {
                mesh = ref[i];
                url = mesh.sage2URL;
                meshExtension = url.split('.').pop();
                if (meshExtension !== 'top') {
                  continue;
                }
                meshFolder = url.substring(0, url.lastIndexOf('/') + 1);
                if (folder !== meshFolder) {
                  continue;
                }
                filenames.push(url);
              }
              return _this.engine.load({
                filenames: filenames
              });
            } else {
              numberStarts = [];
              lastWasDigit = false;
              for (index = j = 0, ref1 = filename.length; 0 <= ref1 ? j < ref1 : j > ref1; index = 0 <= ref1 ? ++j : --j) {
                charCode = filename.charCodeAt(index);
                isDigit = (48 <= charCode && charCode <= 57);
                if (isDigit && !lastWasDigit) {
                  numberStarts.push(index);
                }
                lastWasDigit = isDigit;
              }
              if (numberStarts.length) {
                bestFrameSet = {
                  frames: {},
                  frameLengths: {},
                  length: 0
                };
              } else {
                bestFrameSet = {
                  frames: {
                    0: true
                  },
                  frameLengths: {
                    1: true
                  },
                  startFrame: 0,
                  endFrame: 0,
                  length: 1,
                  numberStartIndex: -1
                };
              }
              for (k = 0, len1 = numberStarts.length; k < len1; k++) {
                numberStartIndex = numberStarts[k];
                frameSet = {
                  numberStartIndex: numberStartIndex,
                  frames: {},
                  frameLengths: {}
                };
                startString = targetFile.substring(0, filenameStartIndex + numberStartIndex);
                originalFileNumber = null;
                ref2 = fileListData.meshes;
                for (l = 0, len2 = ref2.length; l < len2; l++) {
                  mesh = ref2[l];
                  url = mesh.sage2URL;
                  if (url.indexOf(startString) === 0) {
                    numberString = "";
                    index = filenameStartIndex + numberStartIndex;
                    while ((48 <= (ref3 = url.charCodeAt(index)) && ref3 <= 57)) {
                      numberString += url.charAt(index);
                      index++;
                    }
                    if (!numberString.length) {
                      continue;
                    }
                    number = parseInt(numberString);
                    if (!frameSet.frames[number]) {
                      frameSet.frames[number] = true;
                      frameSet.frameLengths[numberString.length] = true;
                    }
                  }
                  if (url === targetFile) {
                    originalFileNumber = number;
                  }
                }
                startFrame = endFrame = originalFileNumber;
                while (frameSet.frames[startFrame - 1]) {
                  startFrame--;
                }
                while (frameSet.frames[endFrame + 1]) {
                  endFrame++;
                }
                frameSet.startFrame = startFrame;
                frameSet.endFrame = endFrame;
                frameSet.length = endFrame - startFrame + 1;
                if (frameSet.length > bestFrameSet.length) {
                  bestFrameSet = frameSet;
                }
              }
              frameNumberLengths = _.keys(bestFrameSet.frameLengths);
              frameNumberLength = frameNumberLengths.length > 1 ? null : parseInt(frameNumberLengths[0]);
              filename = targetFile;
              if (bestFrameSet.numberStartIndex > -1) {
                index = filenameStartIndex + bestFrameSet.numberStartIndex;
                while ((48 <= (ref4 = filename.charCodeAt(index)) && ref4 <= 57)) {
                  filename = "" + (filename.substring(0, index)) + (filename.substring(index + 1));
                }
                filename = (filename.substring(0, index)) + "#" + (filename.substring(index));
              }
              return _this.engine.load({
                filename: filename,
                frameNumberLength: frameNumberLength,
                startFrame: bestFrameSet.startFrame,
                endFrame: bestFrameSet.endFrame
              });
            }
          }
        };
      })(this));
    },
    load: function(date) {},
    draw: function(date) {
      return this.needsDraw = date;
    },
    resize: function(date) {
      return this.needsResize = date;
    },
    drawLoop: function() {
      var date, elapsedTime, time;
      requestAnimationFrame((function(_this) {
        return function() {
          if (!_this._shouldQuit) {
            return _this.drawLoop();
          }
        };
      })(this));
      if (this.needsResize) {
        this.width = this.element.clientWidth;
        this.height = this.element.clientHeight;
        this.engine.resize(this.width, this.height);
        $(this.element).css({
          fontSize: this.height * 0.015
        });
        this.refresh(this.needsResize);
        this.needsResize = false;
      }
      if (this.needsDraw) {
        date = this.needsDraw;
        time = date.getTime() / 1000;
        elapsedTime = time - this.lastDrawTime;
        this.engine.draw(elapsedTime);
        this.needsDraw = false;
        return this.lastDrawTime = time;
      }
    },
    event: function(eventType, position, user_id, data, date) {
      if (eventType === 'pointerPress') {
        this.engine.onMouseDown(position, data.button);
        this.refresh(date);
      } else if (eventType === 'pointerMove') {
        this.engine.onMouseMove(position);
        this.refresh(date);
      } else if (eventType === 'pointerRelease') {
        this.engine.onMouseUp(position, data.button);
        this.refresh(date);
      }
      if (eventType === 'pointerScroll') {
        this.engine.onMouseScroll(data.wheelDelta);
        this.refresh(date);
      }
      if (eventType === 'keyboard') {
        if (data.character === ' ') {
          this.engine.playbackControls.togglePlay();
          this.refresh(date);
        }
      }
      if (eventType === 'specialKey') {
        if (data.state === 'down') {
          switch (data.code) {
            case 37:
              this.engine.playbackControls.previousFrame();
              break;
            case 39:
              this.engine.playbackControls.nextFrame();
              break;
            case 83:
              this.engine.toggleShadows();
              break;
            case 67:
              this.engine.activeControls = this.engine.cameraControls;
              break;
            case 79:
              this.engine.activeControls = this.engine.rotateControls;
              break;
            case 82:
              this.engine.toggleReflections();
              break;
            case 65:
              this.engine.toggleAmbientLight();
              break;
            case 68:
              this.engine.toggleDirectionalLight();
              break;
            case 86:
              this.engine.toggleVertexColors();
              break;
            case 87:
              this.engine.toggleWireframe();
          }
          return this.refresh(date);
        }
      } else if (eventType === 'widgetEvent') {
        switch (data.identifier) {
          case 'Up':
            this.engine.orbitControls.pan(0, this.engine.orbitControls.keyPanSpeed);
            this.engine.orbitControls.update();
            break;
          case 'Down':
            this.engine.orbitControls.pan(0, -this.engine.orbitControls.keyPanSpeed);
            this.engine.orbitControls.update();
            break;
          case 'Left':
            this.engine.orbitControls.pan(this.engine.orbitControls.keyPanSpeed, 0);
            this.engine.orbitControls.update();
            break;
          case 'Right':
            this.engine.orbitControls.pan(-this.engine.orbitControls.keyPanSpeed, 0);
            this.engine.orbitControls.update();
            break;
          case 'ZoomIn':
            this.engine.orbitControls.scale(4);
            break;
          case 'ZoomOut':
            this.engine.orbitControls.scale(-4);
            break;
          case 'Loop':
            this.rotating = !this.rotating;
            this.engine.orbitControls.autoRotate = this.rotating;
            break;
          default:
            console.log('No handler for:', data.identifier);
            return;
        }
        return this.refresh(date);
      }
    },
    move: function(date) {
      return this.refresh(date);
    },
    quit: function() {
      console.log("Destroying meshViewer");
      this._shouldQuit = true;
      this.engine.destroy();
      this.engine = null;
      return this.log('Done');
    }
  });

}).call(this);

//# sourceMappingURL=meshviewer.js.map
