'use strict'

class MeshViewer.Animation
  constructor: (@options) ->
    @frames = []
    @length = 0
    @readyLength = 0
    @ready = false
    @maxConcurrentFramesLoading = @options.maxConcurrentFramesLoading or navigator.hardwareConcurrency or 16

  destroy: ->
    console.log "disposing frames", @frames
    frame.mesh.geometry.dispose() for frame in @frames
    @frames.length = 0
    @frames = null

  getFrameSet: (frameIndex) ->
    [@frames[frameIndex]]

  normalizeMeshes: (forceNormalize, targetFrameSet, referenceAnimation) ->
    referenceAnimation ?= @
    reference = referenceAnimation.frames[0]

    #console.log "using reference frame", reference

    # Make sure we have the reference frame.
    return unless reference?.loaded

    # Make sure the reference has some vertices.
    return unless reference.mesh.geometry.attributes.position.count

    if not reference.normalized or forceNormalize
      # Calculate the reference matrix.
      boundingSphere = reference.mesh.geometry.boundingSphere

      center = boundingSphere.center.clone()
      center.negate()

      translation = new THREE.Matrix4()
      translation.makeTranslation center.x, center.y, center.z
      referenceAnimation.translationY = center.y

      rotation = new THREE.Matrix4()
      rotation.copy referenceAnimation.options.rotation if referenceAnimation.options.rotation

      referenceAnimation.scaleFactor = 1 / boundingSphere.radius
      scale = new THREE.Matrix4()
      scale.makeScale referenceAnimation.scaleFactor, referenceAnimation.scaleFactor, referenceAnimation.scaleFactor

      reference.mesh.matrix.copy rotation
      reference.mesh.matrix.multiply scale
      reference.mesh.matrix.multiply translation

      reference.normalized = true

    matrix = reference.mesh.matrix

    console.log matrix.elements

    # Go over all the frames and normalize those that aren't.
    frames = if targetFrameSet then targetFrameSet else @frames
    for frame in frames
      continue unless frame.loaded and (not frame.normalized or forceNormalize)

      #console.log "normalizing frame"

      frame.mesh.matrix = matrix
      frame.normalized = true

  updateAnimation: ->
    # Set frames to their ready state.
    for frame in @frames
      frame.ready = frame.loaded and frame.normalized

    # See if we have at least the starting frame ready.
    @ready = @frames[0].ready
    return unless @ready

    # Find the last ready frames.
    @readyLength = 1

    while @frames[@readyLength]?.ready
      @readyLength++

    @onUpdated() if @onUpdated
