class MeshViewer.CheckboxControl
  constructor: (@uiArea, @options) ->
    console.log "checkbox", @options

    @value = @options.value

    @$element = $("""
      <div class="checkbox-control #{@options.class}">
        <span class="value"></span> <span class="name">#{@options.name}</span>
      </div>
    """)

    @$value = @$element.find('.value')

    @options.$parent.append(@$element)

    # Create the top UI control for hovering purposes.
    new MeshViewer.UIControl @uiArea, @$element

    valueControl = new MeshViewer.UIControl @uiArea, @$value

    valueControl.mousedown (position) =>
      @setValue not @value

  setValue: (value) ->
    @value = value

    if value
      @$value.addClass('true').removeClass('false')

    else
      @$value.addClass('false').removeClass('true')

    @options.onChange?(@value)
