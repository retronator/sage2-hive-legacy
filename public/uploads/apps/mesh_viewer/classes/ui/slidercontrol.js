// Generated by CoffeeScript 1.10.0
(function() {
  MeshViewer.SliderControl = (function() {
    function SliderControl(uiArea, options) {
      var base, base1, base2, base3, sliderControl;
      this.uiArea = uiArea;
      this.options = options;
      console.log("slider", this.options);
      if ((base = this.options).minimumValue == null) {
        base.minimumValue = 0;
      }
      if ((base1 = this.options).maximumValue == null) {
        base1.maximumValue = 100;
      }
      if ((base2 = this.options).value == null) {
        base2.value = 0;
      }
      if ((base3 = this.options).decimals == null) {
        base3.decimals = 0;
      }
      this.$element = $("<div class=\"slider-control " + this.options["class"] + "\">\n  <span class=\"value\"><span class=\"number\"></span></span>\n  <div class=\"slider\">\n    <div class=\"track\">\n      <div class=\"thumb\"></div>\n    </div>\n  </div>\n</div>");
      this.$value = this.$element.find('.value');
      this.$number = this.$value.find('.number');
      this.$slider = this.$element.find('.slider');
      this.$sliderThumb = this.$slider.find('.thumb');
      if (this.options.unit) {
        this.$value.append(" <class ='unit'>" + this.options.unit + "</div>");
      }
      this.options.$parent.append(this.$element);
      new MeshViewer.UIControl(this.uiArea, this.$element);
      sliderControl = new MeshViewer.UIControl(this.uiArea, this.$slider);
      sliderControl.mousedown((function(_this) {
        return function(position) {
          _this._sliderChanging = true;
          return _this.handleSlider(position);
        };
      })(this));
      sliderControl.mousemove((function(_this) {
        return function(position) {
          if (_this._sliderChanging) {
            return _this.handleSlider(position);
          }
        };
      })(this));
      sliderControl.globalMouseup((function(_this) {
        return function() {
          return _this._sliderChanging = false;
        };
      })(this));
      this.changeSlider(this.options.value);
    }

    SliderControl.prototype.handleSlider = function(position) {
      var mouseXBrowser, rangePercentage, sliderX, unclampedValue;
      mouseXBrowser = this.uiArea.$appWindow.offset().left + position.x;
      sliderX = mouseXBrowser - (this.$slider.offset().left + 5);
      rangePercentage = sliderX / (this.$slider.width() - 10);
      unclampedValue = this.options.minimumValue + (this.options.maximumValue - this.options.minimumValue) * rangePercentage;
      return this.changeSlider(unclampedValue);
    };

    SliderControl.prototype.changeSlider = function(value) {
      var base, thumbPercentage;
      this.value = Math.max(this.options.minimumValue, Math.min(Math.round10(value, this.options.decimals), Math.max(this.options.maximumValue)));
      this.$slider.value = this.value;
      this.$number.text(this.value);
      thumbPercentage = 100.0 * (this.value - this.options.minimumValue) / (this.options.maximumValue - this.options.minimumValue);
      this.$sliderThumb.css({
        left: thumbPercentage + "%"
      });
      return typeof (base = this.options).onChange === "function" ? base.onChange(this.value) : void 0;
    };

    return SliderControl;

  })();

}).call(this);

//# sourceMappingURL=slidercontrol.js.map
