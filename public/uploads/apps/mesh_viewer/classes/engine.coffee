'use strict'

class MeshViewer.Engine

  constructor: (@options)->
    @scene = new MeshViewer.Scene
      engine: @
      resourcesPath: @options.resourcesPath

    @camera = new THREE.PerspectiveCamera(45, @options.width / @options.height, 0.001, 100)
    @camera.position.z = 3

    @renderer = new THREE.WebGLRenderer
      antialias: true

    @renderer.setSize window.innerWidth, window.innerHeight
    @renderer.setClearColor 0x444550

    @renderer.shadowMap.enabled = true
    @renderer.shadowMap.type = THREE.PCFSoftShadowMap

    @$appWindow = @options.$appWindow
    @$appWindow.append @renderer.domElement

    proxyCube = new THREE.Mesh new THREE.BoxGeometry(1, 1, 1), new THREE.MeshLambertMaterial color: 0xeeeeee
    #@scene.add proxyCube

    @cameraControls = new THREE.OrbitControls @camera, @renderer.domElement
    @cameraControls.minDistance = 0.05
    @cameraControls.maxDistance = 10
    @cameraControls.zoomSpeed = 0.5
    @cameraControls.rotateSpeed = 2
    @cameraControls.autoRotate = false
    @cameraControls.autoRotateSpeed = 2.0

    @rotateControls = new THREE.OrbitControls proxyCube, @renderer.domElement
    @rotateControls.enableZoom = false
    @rotateControls.enablePan = false
    @rotateControls.minDistance = 0.05
    @rotateControls.maxDistance = 10
    @rotateControls.rotateSpeed = 1
    @rotateControls.autoRotate = false
    @rotateControls.autoRotateSpeed = 2.0

    @activeControls = @cameraControls

    @loadingManager = new THREE.LoadingManager()

    @shadows = true
    @vertexColors = true
    @reflections = true
    @directionalLight = true
    @ambientLight = true

    @uiAreas = []

    @_frameTime = 0
    @_frameCount = 0

  destroy: ->
    console.log "Destroying engine"

    @scene.showFrameSet null
    @renderer.render @scene, @camera

    @scene.destroy()

    @playbackControls.destroy()
    @playbackControls = null

    @currentAnimation.destroy()
    @currentAnimation = null

    @scene = null
    @camera = null
    @renderer = null

  toggleShadows: ->
    @shadows = not @shadows
    @scene.update()

  toggleVertexColors: ->
    @vertexColors = not @vertexColors
    @scene.update()

  toggleReflections: ->
    @reflections = not @reflections
    @scene.update()

  toggleDirectionalLight: ->
    @directionalLight = not @directionalLight
    @scene.update()

  toggleAmbientLight: ->
    @ambientLight = not @ambientLight
    @scene.update()

  toggleWireframe: ->
    @renderingControls.wireframeControl.setValue not @renderingControls.wireframeControl.value

  resize: (width, height) ->
    @camera.aspect = width / height
    @camera.updateProjectionMatrix()
    @renderer.setSize width, height
    @renderer.setViewport 0, 0, @renderer.context.drawingBufferWidth, @renderer.context.drawingBufferHeight

  load: (options) ->
    console.log "loading animation with options", options

    options = $.extend {}, options,
      loadingManager: @loadingManager
      # Limit concurrency to 4 since big models break our memory limits.
      maxConcurrentFramesLoading: 4
      scene: @scene
      resourcesPath: @options.resourcesPath

    if options.filenames
      @currentAnimation = new MeshViewer.AnimationSet options

    else
      @currentAnimation = new MeshViewer.AnimationSequence options

    @playbackControls = new MeshViewer.PlaybackControls @
    @renderingControls = new MeshViewer.RenderingControls @

    @currentAnimation.options.playbackControls = @playbackControls
    @currentAnimation.options.renderingControls = @renderingControls

    @uiAreas.push @playbackControls
    @uiAreas.push @renderingControls

    @currentAnimation.load()

  draw: (elapsedTime) ->
    @uiControlsActive = false

    for uiArea in @uiAreas
      @uiControlsActive = true if uiArea.rootControl.hover

    unless @currentAnimation
      @renderer.render @scene, @camera
      return

    @playbackControls.update elapsedTime

    if @activeControls is @rotateControls
      @rotateControls.update()
      azimuthal = @rotateControls.getAzimuthalAngle()
      polar = -@rotateControls.getPolarAngle()
      euler = new THREE.Euler polar, azimuthal, 0, 'XYZ'
      @currentAnimation.options.rotation = new THREE.Matrix4().makeRotationFromEuler euler

    else if @activeControls is @cameraControls
      @cameraControls.update()

    unless @currentAnimation.ready
      @renderer.render @scene, @camera
      return

    currentFrameSet = @currentAnimation.getFrameSet @playbackControls.currentFrame
    @currentAnimation.normalizeMeshes true, currentFrameSet if @activeControls is @rotateControls

    @scene.showFrameSet currentFrameSet
    @renderer.render @scene, @camera

    @_frameCount++
    @_frameTime += elapsedTime

    if @_frameTime > 1
      #console.log "FPS:", @_frameCount
      @_frameCount = 0
      @_frameTime = 0

  onMouseDown: (position, button) ->
    console.log "active?", @uiControlsActive
    @activeControls.mouseDown position.x, position.y, @buttonIndexFromString button unless @uiControlsActive

    uiArea.onMouseDown position, button for uiArea in @uiAreas

  onMouseMove: (position) ->
    @activeControls.mouseMove position.x, position.y unless @uiControlsActive

    uiArea.onMouseMove position for uiArea in @uiAreas

  onMouseUp: (position, button) ->
    @activeControls.mouseUp position.x, position.y, @buttonIndexFromString button unless @uiControlsActive

    uiArea.onMouseUp position, button for uiArea in @uiAreas

  onMouseScroll: (delta) ->
    @activeControls.scale delta unless @uiControlsActive

  buttonIndexFromString: (button) ->
    if (button == 'right') then 2 else 0
