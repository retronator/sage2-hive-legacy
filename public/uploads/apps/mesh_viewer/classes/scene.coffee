'use strict'

class MeshViewer.Scene extends THREE.Scene
  constructor: (options) ->
    super()

    @engine = options.engine

    # Create the fancy scene.
    @skyLight = new THREE.HemisphereLight 0x92b6ee, 0x333333, 0.8
    @add @skyLight

    @whiteLight = new THREE.AmbientLight 0xffffff
    @add @whiteLight

    @directionalLight = @addShadowedLight 0.2, 1, 0.3, 0xeadbad, 1
    #@addShadowedLight 1, 1, 1, 0xffffff, 0.5

    cubePath = options.resourcesPath + "cubemaps/aboveclouds/"
    urls = [
      cubePath + "px.jpg"
      cubePath + "nx.jpg"
      cubePath + "py.jpg"
      cubePath + "ny.jpg"
      cubePath + "pz.jpg"
      cubePath + "nz.jpg"
    ]

    @cubeTextureLoader = new THREE.CubeTextureLoader()
    @textureCube = @cubeTextureLoader.load urls

    @planeMaterial = new THREE.MeshBasicMaterial
      color: 0x444550

    @modelMaterial = new THREE.MeshLambertMaterial
      color: 0xffffff
      vertexColors: THREE.VertexColors
      side: THREE.DoubleSide
      envMap: @textureCube
      combine: THREE.MultiplyOperation
      reflectivity: 0.3

    @wireframeMaterial = new THREE.MeshBasicMaterial
      color: 0xffffff
      opacity: 0.5
      transparent: true
      side: THREE.DoubleSide
      wireframe: true

    @plane = new THREE.PlaneGeometry(20, 20)
    @floor = new THREE.Mesh @plane, @planeMaterial
    @floor.rotation.x = -Math.PI * 0.5
    @floor.rotation.z = -Math.PI * 0.5
    @floor.receiveShadow = true;
    @add @floor

    @_currentFrameSet = []

  destroy: ->
    console.log "Destroying scene"

    @skyLight = null
    @directionalLight = null
    @whiteLight = null

    @remove @children...

    frame.mesh.visible = false for frame in @_currentFrameSet
    @_currentFrame = null

    @modelMaterial.dispose()
    @planeMaterial.dispose()
    @modelMaterial = null
    @planeMaterial = null

  addShadowedLight: (x, y, z, color, intensity) ->
    directionalLight = new THREE.DirectionalLight color, intensity
    directionalLight.position.set x, y, z
    @add directionalLight

    directionalLight.castShadow = true
    # directionalLight.shadowCameraVisible = true;
    d = 3
    directionalLight.shadowCameraLeft = -d
    directionalLight.shadowCameraRight = d
    directionalLight.shadowCameraTop = d
    directionalLight.shadowCameraBottom = -d
    directionalLight.shadowCameraNear = 0.0005
    directionalLight.shadowCameraFar = 50
    directionalLight.shadowMapWidth = 1024
    directionalLight.shadowMapHeight = 1024
    directionalLight.shadowDarkness = 0.8
    #directionalLight.shadowBias = -0.001

    directionalLight

  addFrame: (frame) ->
    minY = (frame.mesh.geometry.boundingBox.min.y + @engine.currentAnimation.translationY) * @engine.currentAnimation.scaleFactor
    @floor.position.y = minY

    frame.mesh.visible = false
    @add frame.mesh

  showFrameSet: (frameSet) ->
    #console.log "showing frame set"

    frameSet ?= []

    # Hide frames in the current set.
    frame.mesh.visible = false for frame in @_currentFrameSet

    # Show frames in the new set.
    frame.mesh.visible = true for frame in frameSet

    wireframe = @engine.renderingControls.wireframeControl.value

    if wireframe
      for frame in @_currentFrameSet
        frame.wireframeMesh?.visible = false

      for frame in frameSet
        @generateWireframeMesh frame
        frame.wireframeMesh.visible = true

    #console.log "setting current frame set", frameSet

    @_currentFrameSet = frameSet

    @update()

  generateWireframeMesh: (frame) ->
    if frame and not frame.wireframeMesh
      frame.wireframeMesh = new THREE.Mesh frame.mesh.geometry, @wireframeMaterial
      @add frame.wireframeMesh
      frame.wireframeMesh.applyMatrix frame.mesh.matrix

  update: ->
    return unless @_currentFrameSet.length

    frame.mesh.castShadow = @engine.shadows and @engine.directionalLight for frame in @_currentFrameSet

    @directionalLight.visible = @engine.directionalLight

    @whiteLight.visible = not @engine.ambientLight
    @skyLight.visible = @engine.ambientLight

    materialNeedsUpdate = false

    if @engine.vertexColors isnt @_vertexColor
      @modelMaterial.vertexColors = if @engine.vertexColors then THREE.VertexColors else THREE.NoColors
      @_vertexColor = @engine.vertexColors
      materialNeedsUpdate = true

    if @engine.reflections isnt @_reflections
      @modelMaterial.envMap = if @engine.reflections then @textureCube else null
      @_reflections = @engine.reflections
      materialNeedsUpdate = true

    wireframe = @engine.renderingControls.wireframeControl.value

    if wireframe isnt @_wireframe
      for frame in @_currentFrameSet
        if wireframe
          @generateWireframeMesh frame
          frame.wireframeMesh.visible = true

        else
          frame.wireframeMesh?.visible = false

      @_wireframe = wireframe

    @modelMaterial.needsUpdate = true if materialNeedsUpdate
