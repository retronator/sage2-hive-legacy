'use strict'

class THREE.TopLoader
  constructor: (manager) ->
    @manager = manager or THREE.DefaultLoadingManager

  setCrossOrigin: (value) ->
    @crossOrigin = value

  load: (url, onLoad, onProgress, onError) ->
    worker = new Worker '/uploads/apps/mesh_viewer/classes/toploader-worker.js'

    worker.onmessage = (message) =>
      switch message.data.type
        when 'progress'
          onProgress message.data.loadPercentage if onProgress

        when 'result'
          console.log "Received", url
          objects = message.data.objects
          onLoad objects

    worker.postMessage
      url: url
      crossOrigin: @crossOrigin
      propertyNameMapping: @propertyNameMapping
