window.plyviewer = SAGE2_App.extend
  init: (data) ->
    console.log "PLY init", data

    # data: contains initialization parameters, such as `x`, `y`, `width`, `height`, and `date`
    @SAGE2Init 'div', data
    @resizeEvents = 'continuous'

    @scene = new THREE.Scene()

    @scene.add new THREE.HemisphereLight 0x92b6ee, 0x333333, 0.8

    @addShadowedLight 0.2, 1, 0.3, 0xeadbad, 0.5
    #@addShadowedLight 1, 1, 1, 0xffffff, 0.5

    cubePath = @resrcPath + "cubemaps/aboveclouds/"
    textureCube = THREE.ImageUtils.loadTextureCube [
      cubePath + "px.jpg"
      cubePath + "nx.jpg"
      cubePath + "py.jpg"
      cubePath + "ny.jpg"
      cubePath + "pz.jpg"
      cubePath + "nz.jpg"
    ]

    @planeMaterial = new THREE.MeshBasicMaterial
      color: 0xffffff

    @modelMaterial = new THREE.MeshLambertMaterial
      color: 0xffffff
      vertexColors: THREE.VertexColors
      envMap: textureCube
      combine: THREE.Multiply
      reflectivity: 0.3

    @plane = new THREE.PlaneGeometry(10, 10)
    @floor = new THREE.Mesh @plane, @planeMaterial
    @floor.rotation.x = -Math.PI * 0.5
    @floor.rotation.z = -Math.PI * 0.5
    @floor.receiveShadow = true;
    @scene.add @floor

    @camera = new THREE.PerspectiveCamera 75, @element.clientWidth / @element.clientHeight, 0.1, 1000
    @camera.position.z = -2
    @camera.position.y = 1

    @orbitControls = new THREE.OrbitControls @camera, @element
    #@orbitControls.maxPolarAngle = Math.PI / 2
    @orbitControls.minDistance = 0.5
    @orbitControls.maxDistance = 10
    @orbitControls.zoomSpeed = 0.5

    @orbitControls.autoRotate = false
    @orbitControls.autoRotateSpeed = 2.0

    #your load code here- update app based on this.state
    loader = new THREE.PLYLoader()
    loader.load data.state.file, (geometry) =>
      geometry.computeFaceNormals()
      geometry.computeVertexNormals()
      geometry.computeBoundingBox()
      console.log geometry

      @mesh = new THREE.Mesh geometry, @modelMaterial
      scale = 1 / geometry.boundingSphere.radius

      position = geometry.boundingSphere.center.clone()
      position.multiplyScalar scale
      @orbitControls.target.copy position

      position.negate()
      @mesh.position.copy position
      @mesh.position.y = -scale * geometry.boundingBox.min.y
      @orbitControls.target.y = scale * (geometry.boundingBox.max.y - geometry.boundingBox.min.y) * 0.5
      @mesh.rotation.set 0, 0, 0

      @mesh.scale.set scale, scale, scale
      @mesh.matrixWorldNeedsUpdate = true
      @mesh.castShadow = true;
      #@mesh.receiveShadow = true;

      console.log @mesh
      @scene.add @mesh

    @renderer = new THREE.WebGLRenderer()
    @renderer.setClearColor new THREE.Color 1, 1, 1
    @renderer.setSize @width, @height
    @renderer.shadowMap.enabled = true
    @renderer.shadowMapType = THREE.PCFSoftShadowMap

    @element.appendChild @renderer.domElement

    @needsResize = data.date

    # Start our own draw loop.
    @drawLoop()

  addShadowedLight: (x, y, z, color, intensity) ->
    directionalLight = new THREE.DirectionalLight color, intensity
    directionalLight.position.set x, y, z
    @scene.add directionalLight

    directionalLight.castShadow = true
    # directionalLight.shadowCameraVisible = true;
    d = 3
    directionalLight.shadowCameraLeft = -d
    directionalLight.shadowCameraRight = d
    directionalLight.shadowCameraTop = d
    directionalLight.shadowCameraBottom = -d
    directionalLight.shadowCameraNear = 0.0005
    directionalLight.shadowCameraFar = 50
    directionalLight.shadowMapWidth = 1024
    directionalLight.shadowMapHeight = 1024
    directionalLight.shadowDarkness = 0.8
    #directionalLight.shadowBias = -0.001

  load: (date) ->
    console.log "PLY load", date

  draw: (date) ->
    @needsDraw = date

  resize: (date) ->
    @needsResize = date

  drawLoop: ->
    requestAnimationFrame =>
      @drawLoop()

    if (@needsResize)
      @width = @element.clientWidth
      @height = @element.clientHeight

      @renderer.setSize @width, @height
      @renderer.setViewport 0, 0, @renderer.context.drawingBufferWidth, @renderer.context.drawingBufferHeight
      @camera.aspect = @width / @height
      @camera.updateProjectionMatrix()

      @refresh @needsResize
      @needsResize = false

    if (@needsDraw)
      @orbitControls.update()
      @renderer.clear()
      @renderer.render @scene, @camera
      @needsDraw = false

  event: (eventType, position, user_id, data, date) ->
    if eventType == 'pointerPress' and data.button == 'left'
      @dragging = true
      @orbitControls.mouseDown position.x, position.y, 0
    else if eventType == 'pointerMove' and @dragging
      @orbitControls.mouseMove position.x, position.y
      @refresh date
    else if eventType == 'pointerRelease' and data.button == 'left'
      @dragging = false
    if eventType == 'pointerScroll'
      @orbitControls.scale data.wheelDelta
      @refresh date
    if eventType == 'keyboard'
      if data.character == ' '
        @rotating = !@rotating
        @orbitControls.autoRotate = @rotating
        @refresh date
    if eventType == 'specialKey'
      if data.code == 37 and data.state == 'down'
        # left
        @orbitControls.pan @orbitControls.keyPanSpeed, 0
        @orbitControls.update()
        @refresh date
      else if data.code == 38 and data.state == 'down'
        # up
        @orbitControls.pan 0, @orbitControls.keyPanSpeed
        @orbitControls.update()
        @refresh date
      else if data.code == 39 and data.state == 'down'
        # right
        @orbitControls.pan -@orbitControls.keyPanSpeed, 0
        @orbitControls.update()
        @refresh date
      else if data.code == 40 and data.state == 'down'
        # down
        @orbitControls.pan 0, -@orbitControls.keyPanSpeed
        @orbitControls.update()
        @refresh date
    else if eventType == 'widgetEvent'
      switch data.identifier
        when 'Up'
          # up
          @orbitControls.pan 0, @orbitControls.keyPanSpeed
          @orbitControls.update()
        when 'Down'
          # down
          @orbitControls.pan 0, -@orbitControls.keyPanSpeed
          @orbitControls.update()
        when 'Left'
          # left
          @orbitControls.pan @orbitControls.keyPanSpeed, 0
          @orbitControls.update()
        when 'Right'
          # right
          @orbitControls.pan -@orbitControls.keyPanSpeed, 0
          @orbitControls.update()
        when 'ZoomIn'
          @orbitControls.scale 4
        when 'ZoomOut'
          @orbitControls.scale -4
        when 'Loop'
          @rotating = !@rotating
          @orbitControls.autoRotate = @rotating
        else
          console.log 'No handler for:', data.identifier
          return

      @refresh date

  move: (date) ->
    # this.sage2_x, this.sage2_y give x,y position of upper left corner of app in global wall coordinates
    # this.sage2_width, this.sage2_height give width and height of app in global wall coordinates
    # date: when it happened
    @refresh date

  quit: ->
    # It's the end
    @log 'Done'
