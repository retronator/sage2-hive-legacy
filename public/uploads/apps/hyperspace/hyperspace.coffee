window.hyperspace = SAGE2_App.extend
  init: (data) ->
    @SAGE2Init "div", data
    @resizeEvents = 'continuous'

    @scene = new THREE.Scene()

    @width = @element.clientWidth
    @height = @element.clientHeight

    console.log @width, @height

    @camera = new THREE.PerspectiveCamera 75, @width / @height, 0.1, 1000
    @camera.position.z = -500
    @camera.lookAt new THREE.Vector3 0, 0, 0

    @futureOffset = 0.05

    @renderer = new THREE.WebGLRenderer antialias: true
    @renderer.setSize @width, @height
    @renderer.setViewport 0, 0, @renderer.context.drawingBufferWidth, @renderer.context.drawingBufferHeight

    @element.appendChild @renderer.domElement

    THREE.ImageUtils.crossOrigin = '';
    map = THREE.ImageUtils.loadTexture("#{@resrcPath}/hyperspace.png")
    map.wrapS = THREE.RepeatWrapping
    map.wrapT = THREE.RepeatWrapping

    material = new THREE.MeshBasicMaterial map: map
    material.side = THREE.DoubleSide

    @topPlane = new THREE.PlaneGeometry(1000, 300)
    @topFaceVertexUvs = @topPlane.faceVertexUvs[0]

    top = new THREE.Mesh @topPlane, material
    top.position.x = -1
    top.rotation.y = Math.PI * 0.5
    top.rotation.z = Math.PI

    @bottomPlane = new THREE.PlaneGeometry(1000, 300)
    @bottomFaceVertexUvs = @bottomPlane.faceVertexUvs[0]

    bottom = new THREE.Mesh @bottomPlane, material
    bottom.position.x = 1
    bottom.rotation.y = -Math.PI * 0.5

    top.position.y = bottom.position.y = -1.8

    for faceVertexUvs in [@topFaceVertexUvs, @bottomFaceVertexUvs]
      for face in faceVertexUvs
        for vertex in face
          vertex.y = vertex.y * 3

    @scene.add top
    @scene.add bottom

    console.log "on init", data.date

    @startTime = data.date.getTime() / 1000

  load: (date) ->
    #your load code here- update app based on this.state

  draw: (date) ->
    time = date.getTime() / 1000 - @startTime
    offset = time / 819.2

    # camera.rotation.z += 0.001

    for faceVertexUvs in [@topFaceVertexUvs, @bottomFaceVertexUvs]
      for face in faceVertexUvs
        for vertex in face
          vertex.x = offset

      faceVertexUvs[0][2].x += @futureOffset
      faceVertexUvs[1][1].x += @futureOffset
      faceVertexUvs[1][2].x += @futureOffset

    @topPlane.uvsNeedUpdate = true
    @bottomPlane.uvsNeedUpdate = true

    @renderer.render @scene, @camera

  resize: (date) ->
    @width = @element.clientWidth
    @height = @element.clientHeight

    console.log "RESIZE", @width, @height

    @renderer.setSize @width, @height
    @renderer.setViewport 0, 0, @renderer.context.drawingBufferWidth, @renderer.context.drawingBufferHeight
    @camera.aspect = @width / @height
    @camera.updateProjectionMatrix()

    @refresh date

  event: (type, position, user, data, date) ->
    # see event handling description below
    # may need to update state here
    # may need to redraw
    @refresh date

  move: (date) ->
    # this.sage2_x, this.sage2_y give x,y position of upper left corner of app in global wall coordinates
    # this.sage2_width, this.sage2_height give width and height of app in global wall coordinates
    # date: when it happened
    @refresh date

  quit: ->
    # It's the end
    @log 'Done'
